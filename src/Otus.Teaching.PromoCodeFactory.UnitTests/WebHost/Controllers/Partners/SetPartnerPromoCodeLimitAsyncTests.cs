﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            SetPartnerPromoCodeLimitRequest request = null;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется; Делим на 2:

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimit_NumberIssuedPromoCodesToZero()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();

            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.Partner)
                .Without(x => x.CancelDate)
                .With(x => x.PartnerId, partnerId)
                .Create();

            var partner = fixture.Build<Partner>()
                .Without(a=>a.PartnerLimits)
                .With(a=>a.Id, partnerId)
                .Do(a => a.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(a => a.PartnerLimits.Add(partnerLimit))
                .Create();

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(a => a.Limit, 2)
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }


        /// <summary>
        /// Если у партнера лимит закончился, то количество не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitEnded_NumberIssuedPromoCodesNotChanged()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();

            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.Partner)
                .With(x => x.CancelDate, new DateTime(2019, 2, 3))
                .With(x => x.PartnerId, partnerId)
                .With(x => x.Limit, 4)
                .Create();

            var partner = fixture.Build<Partner>()
                .Without(a => a.PartnerLimits)
                .With(a => a.Id, partnerId)
                .With(a=>a.IsActive, true)
                .With(a => a.NumberIssuedPromoCodes, 4)
                .Do(a => a.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(a => a.PartnerLimits.Add(partnerLimit))
                .Create();

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(a => a.Limit, 4)
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(4);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimit_OldLimitOff()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();
    
            var NewPartnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.Partner)
                .Without(x => x.CancelDate)
                .With(x => x.PartnerId, partnerId)
                .With(x => x.Limit, 4)
                .Create();

            var partner = fixture.Build<Partner>()
                .Without(a => a.PartnerLimits)
                .With(a => a.Id, partnerId)
                .With(a => a.IsActive, true)
                .With(a => a.NumberIssuedPromoCodes, 1)
                .Do(a => a.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(a => a.PartnerLimits.Add(NewPartnerLimit))
                .Create();

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(a => a.Limit, 4)
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.PartnerLimits.First().CancelDate.Should().BeCloseTo(partner.PartnerLimits.Last().CreateDate);
            partner.PartnerLimits.Last().CancelDate.Should().Be(null);
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsLessThenZero_ReturnsBadRequest()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();

            var NewPartnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.Partner)
                .Without(x => x.CancelDate)
                .With(x => x.PartnerId, partnerId)
                .With(x => x.Limit, 4)
                .Create();

            var partner = fixture.Build<Partner>()
                .Without(a => a.PartnerLimits)
                .With(a => a.Id, partnerId)
                .With(a => a.IsActive, true)
                .With(a => a.NumberIssuedPromoCodes, 1)
                .Do(a => a.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(a => a.PartnerLimits.Add(NewPartnerLimit))
                .Create();

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(a => a.Limit, 0)
                .Create();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }


        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом)
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SaveLimitDB_LimitSavedInDB()
        {
            // Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            var partner = fixture.Build<Partner>()
                .Without(a => a.PartnerLimits)
                .With(a => a.IsActive, true)
                .With(a => a.NumberIssuedPromoCodes, 1)
                .Do(a => a.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Create();

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(a => a.Limit, 4)
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            _partnersRepositoryMock.Verify(p => p.UpdateAsync(partner), Times.Once);
        }

    }
}